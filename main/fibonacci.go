package main

import "fmt"

func fibonacci(x, y, i int) {
	fmt.Println(x)
	if i > 1 {
		fibonacci(y, x+y, i-1)
	}
}
